source ~/.git-prompt.sh
PS1='\[`[ $? = 0 ] && X=3 || X=1; tput setaf $X`\]\u\[`tput sgr0`\] \[`[ $? = 0 ] && X=2 || X=1; tput setaf $X`\]\h\[`tput sgr0`\] $PWD $(__git_ps1) \n\$ '
fortune | cowsay -f $(ls /usr/share/cowsay/cows/ | shuf -n1) | lolcat
if [ -d "/srv/system-tools/bin" ] ; then
    PATH="$PATH:/srv/system-tools/bin"
fi
