alias ins="sudo apt install"
alias apacher="tail /var/log/apache2/error.log"
alias apaches="sudo service apache2 restart"
#alias tims="ssh -p 2201 -A timothy@cp.yourdotser.com; notify-send ssh timothy@cp.yourdotser.com -i terminal"
#alias deploy="ssh -p 2201 -A deployinator@cp.yourdotser.com; notify-send ssh deployinator@cp.yourdotser.com -i terminal"
alias pbcopy="xclip -selection c"
alias pbpaste="xclip -selection clipboard -o"
alias json-format="python -m json.tool | pygmentize -l javascript"
function password_generate { head -c ${1-12} /dev/urandom | base64; }
export -f password_generate
function notify { while read input; do notify-send -i $(ls /usr/share/icons/gnome/32x32/emotes | sed 's/\(.*\)\..*/\1/' | shuf -n1) "message" "$input"; done; }
export -f notify
function clear_apc { curl -XGET https://dotser:google@apc.dotser.com/public/clear/modified.php | python -m json.tool; }
export -f clear_apc
function update_clam { sudo /etc/init.d/clamav-freshclam stop; sudo freshclam -v; sudo /etc/init.d/clamav-freshclam start; }
export -f update_clam
function fix_keyboard { sudo dpkg-reconfigure keyboard-configuration; sudo dpkg-reconfigure console-setup; }
export -f fix_keyboard
function muniprops { echo $1 | grep -o . | xargs uniprops; }
export -f muniprops
function set_terminal_title { PROMPT_COMMAND='echo -ne "\033]0;$1\007"'; }
export -f set_terminal_title
